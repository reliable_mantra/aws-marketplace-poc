Django==3.0.5
dj-database-url==0.5.0
djangorestframework==3.11.0
drf-yasg==1.17.0
gunicorn==20.0.4
psycopg2-binary==2.8.5
whitenoise==5.0.1
boto3==1.12.49
