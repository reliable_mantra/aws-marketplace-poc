import json
import boto3
from django.http import JsonResponse


def ping(request):
    # Create SQS client
    sqs = boto3.client('sqs')
    queue_url = 'https://sqs.us-east-2.amazonaws.com/264623451385/MarketplaceProductSubscriptionQueue'

    messages = []

    # Receive message from SQS queue
    response = sqs.receive_message(
        QueueUrl=queue_url,
        MaxNumberOfMessages=10
    )

    if response['Messages']:
        for message in response['Messages']:
            message_body = json.loads(message['Body'])
            messages.append(message_body)

    return JsonResponse({
        "pong": messages,
        "queue_items": sqs.get_queue_attributes(QueueUrl=queue_url, AttributeNames=['All'])
    })
