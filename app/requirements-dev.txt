black==19.10b0
flake8===3.7.9
pytest==5.4.1
pytest-cov==2.8.1
pytest-django==3.9.0

-r requirements.txt
